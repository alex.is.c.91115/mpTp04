import os

#Menu 
def menu():
    os.system('cls')
    print('***Lista de productos***')
    print('1 - Registrar un producto.')
    print('2 - Mostrar la lista de productos. ')
    print('3 - Mostrar los productos cuyo stock se encuentre en el intervalo [desde, hasta].')
    print('4 - Reponer un stock de cantidad menor a Y.')
    print('5 - Eliminar productos con stock 0.')
    print('6 - Salir.')
    opcion = int(input("Ingrese una opcion."))
    while opcion<1 and opcion>6:
        opcion = int(input("Ingrese una opcion."))
    return opcion

def descripcionProducto():
    descripcion = input('Ingrese una descripcion del producto:')
    while len(descripcion)==0:
        descripcion = input('Ingrese una descripcion del producto:')
    return descripcion


def precioProducto():
    while True:
        try:
            precio=int(input('Ingrese el precio de producto:'))
            while precio<0: 
                 precio=int(input('Ingrese el precio de producto:'))
            return precio
        except ValueError:
            print ("ATENCIÓN: Debe ingresar un precio.")
    


def stockProducto():
     while True:
        try:
            stock=int(input('Ingrese stock del producto:'))
            while stock<0:
                stock=float(input('Ingrese stock del producto:'))
            return stock
        except ValueError:
            print ("ATENCIÓN: Debe ingresar una cantida de stock.")

def cargarProductos():
    productos={}
    resp=1
    while (resp == 1):
        codigo = int(input('Ingrese codigo de producto:'))    
        if codigo not in productos:
            descripcion = descripcionProducto()
            precio = precioProducto()
            stock = stockProducto()
            productos[codigo]=[descripcion,precio,stock]
            print('Producto agregado')
            resp=int(input( 'Desea agregar otro producto? 1-Si/2-No: '))
            while resp!=1 and resp !=2:
                resp=int(input( 'Desea agregar otro producto? 1-Si / 2-No: '))
        else:
           print('Codigo de producto ya existente.')
    return productos
    

def mostrarProductos(productos):
    print("Productos ingresados: ")
    for codigo,valor in productos.items():
        print ("Codigo:",codigo,"| Descripcion:",valor[0],"| Precio:",valor[1],"$","| Stock disponible:",valor[2])


def buscarProductos(productos):
    desde=int(input('Ingrese valor inicial de busqueda: '))
    hasta=int(input('Ingrese valor final de busqueda: '))
    if desde>0 and desde<hasta:
        for codigo,valor in productos.items():
            if desde<=valor[2] and valor[2]<=hasta:
             print ("Codigo:",codigo,"| Descripcion:",valor[0],"| Precio:",valor[1],"$","| Stock disponible:",valor[2])   
    else:
        print("El intervalo debe ser positivo...")


def sumarStock(productos):
    x=int(input('Ingrese cantidad para añadir al stock: '))
    y=int(input('Ingrese stocks a reponer: '))
    if x>0 and y>0:
        for codigo,valor in productos.items():
            if y>valor[2]:
                valor[2]=valor[2]+x
        print("Stock cargado con exito...")        
    else:
        print("Ingrese un valor mayor a 0...")
    return productos

def eliminarProducto(productos):
    guia=productos.copy()
    for codigo,valor in guia.items():
            if valor[2]==0:
                del productos[codigo] 
                print("Productos sin stock borrados con exito...")   
            else:
                print("Stock de todos los productos disponible...")    
    return productos


#Lista
opc=0
productos={}
while opc!= 6:
    opc=menu()
    if opc == 1:
        productos=cargarProductos() 
    elif opc==2:
        if productos:
            mostrarProductos(productos)
            input('Presione una tecla para continuar...')
        else :
            input("Lista de productos vacia...")
    elif opc==3:
        if productos:
            buscarProductos(productos)
            input('Presione una tecla para continuar...')
        else :
             input('Lista de productos vacia...')
    elif opc==4:
        if productos:
            productos=sumarStock(productos)
            input('Presione una tecla para continuar...')
        else :
             input('Lista de productos vacia...')
    elif opc==5:
        if productos:
            productos=eliminarProducto(productos)
            input('Presione una tecla para continuar...')
        else :
             input('Lista de productos vacia...')
    elif opc==6:
        print("Cerrando programa...")
